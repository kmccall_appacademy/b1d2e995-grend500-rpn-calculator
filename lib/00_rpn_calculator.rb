require 'byebug'
class RPNCalculator
  def initialize
    @stack = []
  end

  def push(operand)
    @stack.push(operand.to_f)
  end

  def value
    @stack[-1]
  end

  def plus
    raise 'calculator is empty' if @stack.length < 2
    @stack[-2] += @stack[-1]
    @stack.pop
  end

  def minus
    raise 'calculator is empty' if @stack.length < 2
    @stack[-2] -= @stack[-1]
    @stack.pop
  end

  def times
    raise 'calculator is empty' if @stack.length < 2
    @stack[-2] *= @stack[-1]
    @stack.pop
  end

  def divide
    raise 'calculator is empty' if @stack.length < 2
    @stack[-2] /= @stack[-1]
    @stack.pop
  end

  def tokens(string)
    operations = %w[+ - * /]
    string.split.map { |token| operations.include?(token) ? token.to_sym : token.to_i }
  end

  def evaluate(string)
    tokens(string).each do |token|
      if token.is_a? Integer
        push(token)
      else
        case token
        when :+
          plus
        when :-
          minus
        when :*
          times
        when :/
          divide
        end
      end
    end
    value
  end

end
